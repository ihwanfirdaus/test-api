package id.co.nds.test_api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MainController {
	
	@GetMapping("/service1")
	public String service1 () {
		return "Success from service 1";
	}
	
	@GetMapping("/service2")
	public String service2 () {
		return "Success from service 2";
	}

}
